package com.chengjue.elasticsearch.service;

import com.chengjue.elasticsearch.model.Resource;
import com.github.pagehelper.PageInfo;

/**
 * @Author: Kipeng Huang
 * @Date: 2018-4-23 15:15
 */
public interface ResourceService {

    PageInfo<Resource> resources(int page, int count);

    Long resourcesCount();
    Resource findResourceById(Long id);
}
