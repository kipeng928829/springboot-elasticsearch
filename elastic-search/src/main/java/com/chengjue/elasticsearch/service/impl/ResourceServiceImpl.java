package com.chengjue.elasticsearch.service.impl;

import com.chengjue.elasticsearch.dao.ResourceMapper;
import com.chengjue.elasticsearch.model.Resource;
import com.chengjue.elasticsearch.model.ResourceExample;
import com.chengjue.elasticsearch.service.ResourceService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Kipeng Huang
 * @Date: 2018-4-23 15:17
 */
@Service
public class ResourceServiceImpl implements ResourceService {
    @Autowired
    ResourceMapper resourceMapper;

    @Override
    public  PageInfo<Resource> resources(int page, int count) {
        PageHelper.startPage(page,count);
        List<Resource> resources = resourceMapper.selectByExample(new ResourceExample());
        PageInfo<Resource> pageInfo =  PageInfo.of(resources);
         return pageInfo;
    }

    @Override
    public Long resourcesCount() {
        return resourceMapper.countByExample(new ResourceExample());
    }

    @Override
    public Resource findResourceById(Long id) {
        return resourceMapper.selectByPrimaryKey(id);
    }
}
