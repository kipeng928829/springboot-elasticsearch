package com.chengjue.elasticsearch.repository;

import com.chengjue.elasticsearch.model.Resource;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @Author: Kipeng Huang
 * @Date: 2018-4-23 15:13
 */
public interface ResourceRepository extends ElasticsearchRepository<Resource,Long>{

}
