package com.chengjue.elasticsearch;

import com.chengjue.elasticsearch.model.Resource;
import com.chengjue.elasticsearch.repository.ResourceRepository;
import com.chengjue.elasticsearch.service.ResourceService;
import com.github.pagehelper.PageInfo;
import org.apache.lucene.search.Query;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryShardContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticsearchApplicationTests {
    @Autowired
    ResourceService resourceService;

    @Autowired
    ResourceRepository resourceRepository;


    @Test
    public void contextLoads() {
        long count = resourceService.resourcesCount();
        System.out.println("总数：" + count);

        long start = System.currentTimeMillis();
        //查询数据，插入到elastic search
        int pages = (int) (count / 200);
        int page = 1;

        while (page < pages) {
            PageInfo<Resource> resources = resourceService.resources(1, 200);
            page++;
            List<Resource> list = resources.getList();
            resourceRepository.saveAll(list);
        }
        long end = System.currentTimeMillis();

        System.out.println("执行结束：" + (end - start));
    }


    @Test
    public void search() {
        long start = System.currentTimeMillis();
        resourceRepository.findById(200L);
        long end = System.currentTimeMillis();
        System.out.println("elastic 查询时间：" + (end - start));
        start = System.currentTimeMillis();
        resourceService.findResourceById(200L);
        end = System.currentTimeMillis();
        System.out.println("mysql 查询时间：" + (end - start));

    }


    @Test
    public void searchContent(){
        Iterable<Resource> search = resourceRepository.search(new MatchQueryBuilder("content", "心怡"));
        search.forEach(resource -> {
            System.out.println(resource.getName());
            System.out.println(resource.getContent());
        });
    }

}
